
  import { trigger, transition, state, style, animate } from '@angular/animations';


  export let fade = trigger('fade', [
         // not optimized
         /*         transition('void => *', [
      style({
        backgroundColor : 'yellow',
        opacity : 0
      }),
      animate(2000, style({
        backgroundColor : 'white',
        opacity : 1
      }))
    ]),
    transition('* => void', [
      animate(2000, style({
        opacity : 0,
      }))
    ]) */

         // optimize a little
         // state('void', style({
         //   opacity : 0,
         //   backgroundColor : 'yellow',
         // })),
         // transition('void => *', [
         //   animate(2000, style({
         //     backgroundColor : 'white',
         //     opacity : 1
         //   }))
         // ]),
         // transition('* => void', [
         //   animate(2000)
         // ])

         state(
           'void',
           style({
             opacity: 0,
             backgroundColor: 'yellow',
           })
         ),
         // transition('void => * , * => void', [
         // always better
         // void=>* has an alias :enter (leave)
         // transition('void <=> *', [

         transition(':enter, :leave', [
           animate(
             2000,
             style({
               backgroundColor: 'white',
               opacity: 1,
             })
           ),
         ]),
       ]);
